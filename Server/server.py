from flask import Flask, json, request
import json
import sqlite3
import sys 
import os.path
from time import sleep
import socket
from scapy.all import *
from flask_cors import CORS, cross_origin
import random
import _thread as thread
import subprocess
#importing relevent modules

BASE_DIR = os.path.dirname(os.path.abspath(__file__))
db_path = os.path.join(BASE_DIR, "SMS.db") 
con = sqlite3.connect(db_path, check_same_thread=False) 
cur = con.cursor()
#set base to able sql commands 

api = Flask(__name__)
#define the api 

cors = CORS(api)
api.config['CORS_HEADERS'] = 'Content-Type'
#enable front end communication 

@api.route('/GetId', methods=['GET'])
def RandId(): #1 is agent
    type = request.args['Type']
    id = random.randint(1000, 9999)
    temp = []
    sqlCom="select * from managers where manID = "
    if type == '1':
        sqlCom='select * from agents where agentID = '
    while True :
        if (idDoesntExists(sqlCom,id)) : #means no users with this ID
            return str(id)
        id = random.randint(1000, 9999)
#gives a user random id based on his mode 

def idDoesntExists(sqlCom,id):
    comm= sqlCom + str(id) + ';'
    cur.execute(comm )
    temp = cur.fetchall() #the returned value from the db
    if temp == [] : #means no users with this ID
        return True
    return False


@api.route('/', methods=['GET'])
def Welcome():
    return "SMS server"


@api.route('/signAsAgent', methods=['GET'])
def signAsAgent():
    id = request.args['id']
    email = request.args['Email']
    password = request.args['Password']
    if(idDoesntExists('select * from agents where agentID = ',id)):
        sqlComm= 'insert into agents values(' + str(id) +', "' + email+ '", "' + password + '");'
        cur.execute(sqlComm)
        con.commit()
        if id != 0:
            return "done"
    return "an error occurred"
#insert agent to db

@api.route('/signAsManager', methods=['GET'])
def signAsMan():
    id = request.args['id']
    email = request.args['Email']
    password = request.args['Password']
    if(idDoesntExists('select * from managers where manID = ',id)):
        sqlComm= 'insert into managers values(' + str(id) +', "' + email+ '", "' + password + '");'
        cur.execute(sqlComm)
        con.commit()
        if id != 0:
            return "done"
    return "an error occurred"
#insert manager to db

@api.route('/GetAllUsers', methods=['GET'])
def Users():
    cur.execute("SELECT * from Agents ;")
    agents = json.dumps(cur.fetchall())
    return agents
#return all sign users in db 

def AddMissionsToDB(ManID, AgentID, Mission):
    cur.execute('insert into Missions values(' + ManID +', ' + AgentID+ ', "' + Mission + '", 0);')
    Id = cur.execute("SELECT last_insert_rowid() from Missions;")
    con.commit()
    id = (cur.fetchall())[0][0]
    return str(id)
#get a mission in a specific format and inserts it to the data base
    
@api.route('/SendMission', methods=['GET'])
def SendMission():
    ManID = request.args['ManID']
    Mission = request.args['Mission']
    AgentID = request.args['AgentID']
    id = AddMissionsToDB(ManID, AgentID, Mission) # the row id of the mission in the db
    return id


@api.route('/getMission', methods=['GET'])
def getMission():
    agentID = request.args['agentID']
    cur.execute("SELECT rowid,mission,is_done from Missions where agentID = " + str(agentID) + " ;")
    j_data = json.dumps(cur.fetchall()) #(rowid,mission,isdone)
    return j_data

@api.route('/deleteMission', methods=['GET'])
def deleteMission():
    rowid = request.args['rowid']
    cur.execute("DELETE FROM missions WHERE rowid=" + str(rowid) + " ;")
    con.commit()
    return 'OK'
#remove a mission from db by its row id 

@api.route('/checkIfDone', methods=['GET'])
def checkIfDone():

    missionID = request.args['missionID']
    cur.execute("SELECT is_done from Missions where rowid = " + str(missionID) + " ;")
    is_done = cur.fetchone()
    return str(is_done[0])


@api.route('/AnsFromAgent', methods=['POST']) #set answer from agent
def AnsFromAgent():
    ans = request.json.get('process')
    rowid = request.json.get('rowid')
    cur.execute('UPDATE missions set mission ="'+ ans+'" where rowid ='+str(rowid)+' ;')
    cur.execute('UPDATE missions set is_done = 1 where rowid ='+str(rowid)+';')
    con.commit()
    return ""

@api.route('/getAns', methods=['GET']) #manager get answer from the db
def getAns():
    rowid = request.args['rowid']
    cur.execute("SELECT mission from Missions where rowid == " + str(rowid) + " ;")
    j_data = json.dumps(cur.fetchall())
    return j_data


@api.route('/closeServer', methods=['GET'])
def CloseSer():
    con.close()
    sys.exit()


def get_own_ip():
    try:
        proc = subprocess.Popen("ipconfig", shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE)
        out, err = proc.communicate()
        return str(out).split("Wi-Fi 2:")[1].split("IPv4 Address")[1].split(":")[1].split("\\r\\n")[0].replace(" ", "")
    except Exception as e:
        return "127.0.0.1"


def main():
    ip = "127.0.0.1"
    ServerKind = int(input("1) run the server on localhost\n2)run the server on subnet\n "))
    if(ServerKind == 2):
        ip = get_own_ip()
    api.run(host=ip)


if __name__ == '__main__':
    main()
