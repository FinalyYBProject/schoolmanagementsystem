Host = 'http://127.0.0.1:5000'
let manID = 0
//getMyid()
let agentID = 0
presentAllAgents()
//import {saveAs} from 'scripts\FileSaver.js'
var taskBtn = document.getElementById("task-man")
var cmdBtn = document.getElementById("cmd")
var infoBtn = document.getElementById("information")
var fileExpBtn = document.getElementById("file-exp")

const taskDiv = document.querySelector("div.taskList") // Find the taskList div in our html
let tableHeaders = [" ","No.","Image Name", "PID", "Session Name", "Session Number", "Memory (KB)"] // " " is for kill 
var killBtn
var showFolderBtn
var delfilebtn

taskBtn.addEventListener("click",tasklst) 
cmdBtn.addEventListener("click",cmdComm)
infoBtn.addEventListener("click",basicInfo) // return to basicInfo
fileExpBtn.addEventListener("click",fileExp)//return to fileExp



////////////////////////////////////////////////////////////////
////////////////signing in/////////////////////////////

function getMyid() 
{ 
  var xhttp = new XMLHttpRequest();
  xhttp.onload = function() 
  {
    manID = this.responseText
    signAsManager(manID)
  }
  FlaskQuery = Host+"/GetId?Type=0"
  xhttp.open("GET", FlaskQuery);
  xhttp.send();
}

function signAsManager(id) 
{ 
  var xhttp = new XMLHttpRequest();
  xhttp.onload = function() 
  {
    console.log((this.responseText))
    
  }
  FlaskQuery = Host+"/signAsManager?id="+id.toString()+"&Email='dvirmalka10@gmai.com'&Password='dvir'"
  xhttp.open("GET", FlaskQuery);
  xhttp.send();
}

//////////////////////////////////////////////////
//////////////////////////////////////////////////////////
//select agent to work with

function presentAllAgents()
{
  var xhttp = new XMLHttpRequest();
  xhttp.onload = function() 
  {
    agents =(this.responseText).split("], [")
    showAgents(agents)
  }
  FlaskQuery = Host+"/GetAllUsers"
  xhttp.open("GET", FlaskQuery);
  xhttp.send();
}


function showAgents(agents)
{
  cleanScreen()
  agentDiv = document.getElementById("Agents")
  msgDiv = document.createElement("div")
  msgDiv.innerText ="Please choose an Agent"
  msgDiv.id = "msg-div"
  agentDiv.appendChild(msgDiv)
  for (var i=0 ;i<agents.length;i++)
  {
    agent_info = agents[i].split(",")
    
    addAgentToScreen(agent_info)
  }
  agentID = agents[0].split(",")[0].slice(2)
}

function addAgentToScreen(agent)
{
  if(agent[0][0] === '['){agent[0] = agent[0].slice(2)}
  agentDiv = document.getElementById("Agents")
  a_btn = document.createElement("button")
  a_btn.className = "agent-btn"
  a_btn.innerText = agent[1]
  a_btn.id = agent[0]
  a_btn.addEventListener("click",setAgentId)
  agentDiv.appendChild(a_btn)
}

function setAgentId()
{
  cleanScreen()
  agentID = this.id
}



  //-----------------------------------------------------------------------------//
 //----------------------------3 BASIC FUNCTIONS--------------------------------//
//-----------------------------------------------------------------------------//
function insertMission(mission) // gets any kind of mission and insert to the db
{ 
  var xhttp = new XMLHttpRequest();
  xhttp.onload = function() 
  {
    um=document.getElementById("userMSG")
    um.className = "loader"
    document.getElementById("taskList").innerHTML = ""
    document.getElementById("basicInfo").innerHTML = ""
    document.getElementById("basicInfo").innerHTML = ""
    checkIfDone(this.responseText,mission)
  }
  FlaskQuery = Host+"/SendMission?AgentID=" + agentID.toString() +"&"+ "ManID="+ manID.toString() +"&"+ "Mission="+mission // +mission.toString()
  xhttp.open("GET", FlaskQuery);
  xhttp.send();
  }


function checkIfDone(rowId,mission)
{ 
  var xhttp = new XMLHttpRequest();
  xhttp.onload = function() 
  {
    if(this.responseText.slice(0,1) === '0')
    {
      setTimeout(() => {  checkIfDone(rowId,mission)}, 1000);
    }
    else
    {
      getAnswer(rowId,mission) 

    }
  }
  FlaskQuery = Host+"/checkIfDone?missionID=" + rowId
  xhttp.open("GET", FlaskQuery);
  xhttp.send();
}

function getAnswer(rowId,mission)/// IMPORTANT change row id
{
    var xhttp = new XMLHttpRequest();
    xhttp.onload = function() 
    {
        if(mission == "GetOpenTasks")
        {
          cleanScreen()
            showTaskList(this.responseText.slice(4,(this.responseText.length-4)))
        }
        else if(mission.includes("KillTask"))
        {
          cleanScreen()
          if(this.responseText == '[["done"]]') { tasklst()}
          else{console.log("ERROR DELETING PROCESS ///")}
        }
        else if(mission == "GetBasicInfo")
        {
          cleanScreen()
          showBasicInfo(this.responseText)
        }
        else if(mission.includes("GetFileExplorer"))
        {
          cleanScreen()
          showFiles(this.responseText,mission.split("=")[1])
        }
        else if(mission.includes("DeleteFile"))
        {
          cleanScreen()
          if(this.responseText == '[["done"]]')
          {
            path = mission.split("=")[1]
            one_back = nevBack(path)
            insertMission("GetFileExplorer path="+one_back)
          }
          else
          {
            console.log("ERROR DELETING " + mission.split("=")[1])
          }
        }
        else if(mission.includes("CMDcommand"))
        {
          var AnsDiv = document.getElementById("AnswerDiv")
          CMD_Ans(this.responseText.slice(2, (this.responseText.length-2)))
        }
      
    }
    FlaskQuery = Host+'/getAns?rowid='+rowId
    xhttp.open("GET", FlaskQuery);
    xhttp.send();
}
  //-----------------------------------------------------------------------------//
 //----------------------------3 BASIC FUNCTIONS--------------------------------//
//-----------------------------------------------------------------------------//




  //-------------------------------------------------------------------//
 //----------------------------HELPERS--------------------------------//
//-------------------------------------------------------------------//
function nevBack(path)
{
  if(path[path.length-1] == '/')
  {
    path=path.slice(0,path.length-1)
  }
  lst = path.split("/")
  one_back = ""
  for (i=0;i<lst.length-1;i++)
  {
    one_back += lst[i]
    one_back += "/"
  }
  return one_back
}
function DcreateItems(dirName)
{
  fileExpDiv= document.getElementById("rem-file-exp");
  folderbtn = document.createElement('button')
  folderbtn.className = "dir-btn"
  folderIcon = document.createElement('span')
  folderIcon.className = "glyphicon glyphicon-folder-open"
  folderIcon.innerText = " "+dirName
  
  folderbtn.appendChild(folderIcon)
  fileExpDiv.appendChild(folderbtn)
}

function FcreateItems(filename,maindir)
{
  fileExpDiv= document.getElementById("rem-file-exp");
  filebtn = document.createElement('button')
  filebtn.className = "file-btn"
  fileIcon = document.createElement('span')
  fileIcon.className = "glyphicon glyphicon-file"
  fileIcon.innerText = " "+filename
  deleteFileBtn = document.createElement('button')
  deleteFileBtn.className = "del-file-btn"
  delIcon = document.createElement('span')
  delIcon.className = "glyphicon glyphicon-trash"
  filebtn.appendChild(fileIcon)
  deleteFileBtn.appendChild(delIcon)
  deleteFileBtn.id = maindir+"/"+filename
  deleteFileBtn.appendChild(filebtn)

  fileExpDiv.appendChild(deleteFileBtn)
}
function cleanScreen()
{
  um = document.getElementById("userMSG")
  um.className = ""
  document.getElementById("taskList").innerHTML = ""
  document.getElementById("rem-file-exp").innerHTML = ""
  document.getElementById("basicInfo").innerHTML = ""
  document.getElementById("CMDcommand").innerHTML = ""
  document.getElementById("Agents").innerHTML = ""
}
function backbtn()
{
  m = document.getElementById("main-dir-div")
  path = m.innerText.split(" ")[0]
  newpath = nevBack(path)
  insertMission("GetFileExplorer path="+newpath)
}

function PressOnInput()
{
  var AnsDiv = document.getElementById("AnswerDiv")
  if (AnsDiv.innerHTML != "")
  {
    AnsDiv.innerHTML = ""
  }
}

function GetUserInput()
{
  if (window.event.keyCode === 13) 
  {
    var UserInput = document.getElementById("inputID").value.toString()
    insertMission("CMDcommand"+UserInput)
  }
}

function CMD_Ans(answer)
{
  var Ans = document.getElementById("AnswerDiv")
  um = document.getElementById("userMSG")
  um.className = ""
  document.getElementById("taskList").innerHTML = ""
  document.getElementById("rem-file-exp").innerHTML = ""
  document.getElementById("basicInfo").innerHTML = ""
  if(answer.includes("Error"))
  {
    answer = (answer.split(".")[0])
    answer =  answer.slice(1,answer.length)
    Ans.innerHTML = answer 
  }
  else
  {

    answer = answer.slice(3, -2)
    answer = answer.replace(/\\\\n/g, "\n")
    answer = answer.replace(/\\\\r/g, "\r")
    answer = answer.replace(/\\\\t/g, "\t")
    answer = answer.replace(/\\\\/g, "\\")
    var str = "" 
    for (var i =0 ; i < answer.split("\n").length ; i++)
    {
      str = str.concat(answer.split("\n")[i])
      str = str.concat("<br>")
    }
    Ans.innerHTML = str 

  }
  
}

  //-------------------------------------------------------------------//
 //----------------------------HELPERS--------------------------------//
//-------------------------------------------------------------------//



  //--------------------------------------------------------------------------//
 //---------------------------RELATED TO BUTTONS-----------------------------//
//--------------------------------------------------------------------------//
function killProcess()
{ 
  cleanScreen()
  insertMission("KillTask taskid="+this.id.toString())
}
function basicInfo()
{
  cleanScreen()
  insertMission("GetBasicInfo")
}
function tasklst()
{
    cleanScreen()
    insertMission("GetOpenTasks")
}
function fileExp()
{
  insertMission("GetFileExplorer path=C:/")
}
function deleteFile()
{
  insertMission("DeleteFile=" + this.id)
}
function showThisFolder()
{
  insertMission("GetFileExplorer path="+this.innerText.slice(1)) 
}
function cmdComm()
{
  cleanScreen()
  var cmdDiv = document.getElementById("CMDcommand")
  var InputDiv = document.createElement("input")
  InputDiv.id = "inputID"
  InputDiv.type = "text"
 // InputDiv.addEventListener('keypress', PressOnInput)
  InputDiv.placeholder = ">>"
  var AnswerDiv = document.createElement("div")
  AnswerDiv.id = "AnswerDiv"
  cmdDiv.appendChild(InputDiv)
  cmdDiv.appendChild(AnswerDiv)


  if(InputDiv){InputDiv.addEventListener("keyup",GetUserInput )}
}
  //--------------------------------------------------------------------//
 //---------------------------RELATED TO BUTTONS-----------------------//
//--------------------------------------------------------------------//


  //-------------------------------------------------------------------//
 //----------------------------FRONT DESIN----------------------------//
//-------------------------------------------------------------------//
const createtaskListTable = () =>
{
      while (taskDiv.firstChild) taskDiv.removeChild(taskDiv.firstChild) // Remove all children from taskList div (if any)
      let taskListTable = document.createElement('table') // Create the table itself
      taskListTable.className = 'taskListTable'
      let taskListTableHead = document.createElement('thead') // Creates the table header group element
      taskListTableHead.className = 'taskListTableHead'
      let taskListTableHeaderRow = document.createElement('tr') // Creates the row that will contain the headers
      taskListTableHeaderRow.className = 'taskListTableHeaderRow'
      // Will iterate over all the strings in the tableHeader array and will append the header cells to the table header row
      tableHeaders.forEach(header => 
      {
          let taskHeader = document.createElement('th') // Creates the current header cell during a specific iteration
          taskHeader.innerText = header
          taskListTableHeaderRow.append(taskHeader) // Appends the current header cell to the header row
      })
  taskListTableHead.append(taskListTableHeaderRow) // Appends the header row to the table header group element
  taskListTable.append(taskListTableHead)
  let taskListTableBody = document.createElement('tbody') // Creates the table body group element
  taskListTableBody.className = "taskListTable-Body"
  taskListTable.append(taskListTableBody) // Appends the table body group element to the table
  taskDiv.append(taskListTable) // Appends the table to the taskList div
}
function showFiles(data,main_dir)
{

  fileExpDiv= document.getElementById("rem-file-exp");
  while (fileExpDiv.firstChild) fileExpDiv.removeChild(fileExpDiv.firstChild) 
  data = data.slice(3,data.length-3)
  data = data.replaceAll("'",'"')
  let j =JSON.parse(data) ///parsing data
  maindirDiv = document.createElement('div')
  maindirDiv.innerText = main_dir
  maindirDiv.id = "main-dir-div"
  back = document.createElement('button')
  back.id = "back-btn"
  backIcon = document.createElement('span')
  backIcon.className = "glyphicon glyphicon-arrow-left"
  back.appendChild(backIcon)
  maindirDiv.appendChild(back)
  fileExpDiv.appendChild(maindirDiv)
  back.addEventListener("click",backbtn)
  for (i=0;i< j.files.length;i++){ FcreateItems(j.files[i],main_dir)}
  for (i=0;i<j.directories.length;i++)
  {
    if(!j.directories[i].includes("$"))
    {
      DcreateItems(j.directories[i])
    }
  }
  showFolderBtn = document.getElementsByClassName("dir-btn") 
  for (var i = 0; i < showFolderBtn.length; i++)
  {
    showFolderBtn[i].addEventListener('click', showThisFolder);
  }
  delfilebtn = document.getElementsByClassName("del-file-btn") 
  for (var i = 0; i < delfilebtn.length; i++) {
    delfilebtn[i].addEventListener('click', deleteFile);
  }
}
//The function below will accept a single task and its index to create the global ranking
const appendtasks = (singletask, singletaskIndex) =>
{
      const taskListTable = document.querySelector('.taskListTable') // Find the table we created
      let taskListTableBodyRow = document.createElement('tr') // Create the current table row
      taskListTableBodyRow.className = 'taskListTableBodyRow'
      // Lines 72-85 create the 5 column cells that will be appended to the current table row
      let taskRanking = document.createElement('td')
      taskRanking.innerText = singletaskIndex
      let image = document.createElement('td')
      image.innerText = singletask.image
      let pid = document.createElement('td')
      pid.innerText = singletask.pid
      let session_name = document.createElement('td')
      session_name.innerText = singletask.session_name
      let session_num = document.createElement('td')
      session_num.innerText = singletask.session_num
      let mem_usage = document.createElement('td')
      mem_usage.innerText = singletask.mem_usage
      let kill = document.createElement("button");
      kill.innerHTML = "KILL"
      kill.className = "killProcessBtn"
      kill.id = singletask.pid
      taskListTableBodyRow.append(kill,taskRanking, image, pid, session_name, session_num,mem_usage) // Append all 5 cells to the table row
      taskListTable.append(taskListTableBodyRow) // Append the current row to the taskList table body
}
function showTaskList(lst)
{
  um = document.getElementById("userMSG")
  um.className = ""
  var array = (new Function("return [" + lst+ "];")());
  gettasks(array) ////// changes 
  killBtn = document.getElementsByClassName("killProcessBtn") 
  for (var i = 0; i < killBtn.length; i++) {
    killBtn[i].addEventListener('click', killProcess);
  }

  
}
const gettasks = (tasks) =>
{
    createtaskListTable() // Clears taskList div if it has any children nodes, creates & appends the table
    // Iterates through all the objects in the tasks array and appends each one to the table body
    for (const task of tasks)
    {
      let taskIndex = tasks.indexOf(task) + 1 // Index of task in task array for global ranking (these are already sorted in the back-end)
      appendtasks(task, taskIndex) // Creates and appends each row to the table body
    }
   
}

function showBasicInfo(info)
{
  let s =info.slice(3, (info.length-3)).toString()
  s=s.replaceAll("'",'"')
  info =JSON.parse(s)
  um = document.getElementById("userMSG");
  um.className = ""
  document.getElementById("taskList").innerHTML="";
  men = document.getElementById("basicInfo")
  men.innerHTML = ""
  infoDiv=document.createElement('div')
  infoDiv.id = "info-div"


  leftSide = document.createElement('div')
  leftSide.className = "left-div-info"
    version = document.createElement('div')
    version.id="version"
    version.innerText = "Version"
    leftSide.appendChild(version)
    architecture = document.createElement('div')
    architecture.id="architecture"
    architecture.innerText = "Architecture"
    leftSide.appendChild(architecture)
    cors_num = document.createElement('div')
    cors_num.id="cors_num"
    cors_num.innerText="Cors Number"
    leftSide.appendChild(cors_num)


  rightSide = document.createElement('div')
  rightSide.className = "right-div-info"
    username = document.createElement('div')
    username.id="username"
    username.innerText="Username"
    rightSide.appendChild(username)
    win_edition = document.createElement('div')
    win_edition.id="win_edition"
    win_edition.innerText="Windows Edition"
    rightSide.appendChild(win_edition)
    cpu = document.createElement('div')
    cpu.id="cpu"
    cpu.innerText = "CPU"
    rightSide.appendChild(cpu)

  infoDiv.appendChild(leftSide)
  infoDiv.appendChild(rightSide)
  men.appendChild(infoDiv)


  version.innerHTML += " : " + info.version
  architecture.innerHTML +=  " : \n" +  info.architecture
  cors_num.innerHTML +=  " : \n" + info.cors_number
  username.innerHTML +=  " : \n" + info.username
  win_edition.innerHTML += " : \n" +  (info.windows_edition)
  cpu.innerHTML +=  " : \n" + (info.cpu)
}

  //-------------------------------------------------------------------//
 //----------------------------FRONT DESIN----------------------------//
//-------------------------------------------------------------------//

