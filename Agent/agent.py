import requests
import os 
import ast
from time import sleep
import signal
import glob
import subprocess
from scapy.all import *
import socket
from multiprocessing import Process, Queue
import time
import cpuinfo
import multiprocessing
import _thread as thread
import platform
from os import listdir
from os.path import isfile, join
import re
from subprocess import Popen, PIPE, check_output

#importing relevent modules


MY_ID = 0
NUM_OF_THREADS = 22
host = ""
subnet = ""
startTime = time.time() #To measure time finding  the server
port = 5000
threadsRes = ['Searching'] * NUM_OF_THREADS 
#programs globals 


def get_processes_running():
    """
    Takes tasklist output and parses the table into a dict
    """
    tasks = check_output(['tasklist']).decode('cp866', 'ignore').split("\r\n")
    p = []
    for task in tasks:
        m = re.match(b'(.*?)\\s+(\\d+)\\s+(\\w+)\\s+(\\w+)\\s+(.*?)\\s.*', task.encode())
        if m is not None and (m.group(5).decode('ascii', 'ignore').replace(',','')).isdigit():
            p.append({"image":m.group(1).decode(),
                        "pid":int(m.group(2).decode().replace(',','')),
                        "session_name":m.group(3).decode(),
                        "session_num":int(m.group(4).decode()), 
                        "mem_usage":int(m.group(5).decode('ascii', 'ignore').replace(',',''))
                        })
    return p


def sendToServer(manID,mission,agentID):
    FlaskQuery = ""
    Host = 'http://' + host + ':5000/SendMission'
    FlaskQuery = "?agentID=" + agentID +"&"+ "manID="+ manID +"&"+ "Mission=" + str(mission)
    return requests.get(Host+FlaskQuery).text

def KillTask(pid):
    tasks = get_processes_running()

    for i in tasks :
        if str(i["pid"]) == str(pid) :
           try:
                os.kill(int(pid), signal.SIGTERM)
                #os.system("taskkill /PID " + str(pid))
                return "done"
           except Exception as e:
               return ("Error: ", e)

    return "task doesnt exist"


def GetBasicInfo():
    info = dict()
    info['windows_edition'] =  (platform.uname())[0]+(platform.uname())[2]
    info['username'] =  platform.uname()[1]  
    info['version'] =  platform.uname()[3]
    info['architecture'] =  platform.architecture()[0]
    info['cpu'] = cpuinfo.get_cpu_info()['brand_raw']
    info['cors_number'] = multiprocessing.cpu_count()
    return info


def GetFileExplorerByFile(file):
    try:
        return glob.glob("/*")
    except Exception as e:
        return ("Error: ", e)


def GetFileExplorer(path):
    try:
        allItems = dict()
        onlyfiles = [f for f in listdir(path) if isfile(join(path, f))]
        allItems["files"] = onlyfiles
        onlyDir = [os.path.join(path, o) for o in os.listdir(path) if os.path.isdir(os.path.join(path,o))]
        allItems["directories"] = onlyDir
        return allItems
    except Exception as e:
        return ("Error: ", e)

def deleteFile(file_path):
    try:
        if os.path.exists(file_path):
            os.remove(file_path)
            return "done"
        else:
            return "file dosen't exists"
    except Exception as e:
        return ("Error: ", e)

def RunCMD(command):
    try:
        proc = subprocess.Popen(command, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE)
        out, err = proc.communicate()
        if(len(out)>0):
            return out
    except Exception as e:
        pass
    return "Error: '"+ str(command) + "':is not recognized as an internal or external command,operable program or batch file. "


def PutAswerInDB(Answer, rowid):
    Ans= str(Answer)
    data = {"process":Ans, "rowid":rowid}
    FlaskQuery = 'http://'+ host +':5000/AnsFromAgent'
    r = requests.post(url = FlaskQuery, json = data)
    return ""


def SplitToCommands(command, rowid):
    if "KillTask taskid=" in command:
        pid = command.split("=")[1] 
        PutAswerInDB(KillTask(pid) , rowid)
    elif "CMDcommand" in command :
        cmd = command[10:]
        PutAswerInDB(RunCMD(cmd), rowid)
    elif "GetFileExplorerByFile file-" in command :
        file = command.split("-")[1]
        PutAswerInDB(GetFileExplorerByFile(file), rowid)
    elif "GetFileExplorer" in command :
        path = command.split("=")[1]
        PutAswerInDB(GetFileExplorer(path), rowid)
    elif "GetOpenTasks" in command :
        PutAswerInDB(get_processes_running(), rowid)
    elif "GetBasicInfo" in command :
        PutAswerInDB(GetBasicInfo(), rowid)
    elif "DeleteFile" in command:
        file_path = command.split("=")[1]
        PutAswerInDB(deleteFile(file_path), rowid)
    return ""
#Receives a command and navigate it to the right function


def GetSubnetMask():
    try: 
        proc = subprocess.Popen("ipconfig", shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE)
        out, err = proc.communicate()
        return str(out).split("Wi-Fi")[1].split("Subnet Mask")[1].split(":")[1].split("\\r\\n")[0].replace(" ", "")
    except Exception as e:
        return "ERROR"
    

def get_own_ip():
    try: 
        proc = subprocess.Popen("ipconfig", shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE)
        out, err = proc.communicate()
        return str(out).split("Wi-Fi")[1].split("IPv4 Address")[1].split(":")[1].split("\\r\\n")[0].replace(" ", "")
    except Exception as e:
        return "ERROR"
    
def getSplitedSubnetMask():
    final = ""
    for i , j in zip(subnet.split("."), get_own_ip().split(".")):
        if i == "255" :
            final = final  + j + "."
    return final

def searchRange(Range, index ,*args):
    x = ""
    #global host
    global threadsRes 
    for i in range(Range[0], Range[1]): 
        if host != "" :
            return 0  
        FlaskQuery = 'http://'+ getSplitedSubnetMask()+str(i) + ':5000/'
        try: 
            requests.get(FlaskQuery, timeout=3).content
            host = getSplitedSubnetMask()+str(i)
            threadsRes[index] = "Found" 
        except Exception as e:
            continue
    threadsRes[index] = "Done"

def FindHost():
    getSplitedSubnetMask()
    count = 0 
    for i in range(1, 254, 12):
        if i+12 > 254 :
            thread.start_new_thread(searchRange, ([i, 254],count, 1))
        else :
            thread.start_new_thread(searchRange, ([i, i+12],count, 1))
        count = count + 1

def connectToServer(option):
    if (option == 1):
        global host
        host = "127.0.0.1"
    elif (option == 2):
        global subnet
        global threadsRes 
        subnet = GetSubnetMask()
        while subnet == "" or subnet == "ERROR":
            sleep(3)
            subnet = GetSubnetMask()
        FindHost()
        while host == "" :
            sleep(3)
            if "Searching" not in threadsRes and "Done" not in threadsRes :
                sleep(5)
                FindHost()
    MY_ID = (requests.get('http://' + host + ':5000/GetId?Type=1').content).decode("utf-8")
    email = input("Email to sign in with: ")
    password = input("Password to sign in with: ")
    requests.get('http://' + host + ':' + str(port) +'/signAsAgent?Email=' + email + '&Password=' + password + "&id=" + str(MY_ID)).content       
    return MY_ID


def main():
    MY_ID = connectToServer(int(input("1) run the server on localhost\n2)run the server on subnet\n ")))
    while(True):
        sleep(10)
        try:
            missions = ast.literal_eval(((requests.get('http://' + host + ':5000/getMission?agentID=' + str(MY_ID))).content).decode("utf-8"))
            if(len(missions) != 0):
                for mission in missions:
                    if(mission[2] == 0):
                        SplitToCommands(mission[1],mission[0])
        except Exception as e:
            print("Error: ", e)

if __name__ == "__main__":
	main()
